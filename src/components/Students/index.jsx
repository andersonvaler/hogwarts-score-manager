import papper from "../../icons/papper.svg";
import { useState } from "react";
import ChangeNote from "../ChangeNote";

const Students = ({ student }) => {
  const [openModal, setOpenModal] = useState(false);
  return (
    <div className="student">
      <div className="name">
        <h3>{student.name}</h3>
      </div>
      <div className="house-name">
        <p>{student.house}</p>

        <figure onClick={() => setOpenModal(!openModal)}>
          <img src={papper} alt="edit" />
        </figure>
      </div>
      <ChangeNote
        openModal={openModal}
        setOpenModal={setOpenModal}
        student={student}
      />
    </div>
  );
};

export default Students;
