import { makeStyles } from "@material-ui/core/styles";
import Modal from "@material-ui/core/Modal";
import Backdrop from "@material-ui/core/Backdrop";
import { useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { addNote, subNote } from "../../store/modules/houses/actions";

const useStyles = makeStyles((theme) => ({
  modal: {
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
  },
}));

const ChangeNote = ({ openModal, setOpenModal, student }) => {
  const houses = useSelector((state) => state.houses);

  const houseSelected = houses.filter((house) => house.name === student.house);

  const dispatch = useDispatch();

  const [inputValue, setInputValue] = useState(0);
  const [graded, setGraded] = useState(false);
  const [add, setAdd] = useState();

  const gradedAdd = () => {
    dispatch(addNote(student.house, Number(inputValue)));
    setGraded(!graded);
    setAdd(true);
  };

  const gradedSub = () => {
    dispatch(subNote(student.house, Number(inputValue)));
    setGraded(!graded);
    setAdd(false);
  };

  const handleInput = (e) => {
    setInputValue(e);
  };

  const classes = useStyles();

  const handleClose = () => {
    setOpenModal(false);
    setInputValue(0);
    setGraded(false);
    setAdd();
  };

  return (
    <div>
      <Modal
        aria-labelledby="transition-modal-title"
        aria-describedby="transition-modal-description"
        className={classes.modal}
        open={openModal}
        onClose={handleClose}
        closeAfterTransition
        BackdropComponent={Backdrop}
        BackdropProps={{
          timeout: 500,
        }}
      >
        <div className="modal">
          <div className="student-image">
            <figure>
              <img src={student.image} alt={student.name} />
            </figure>
          </div>
          <div className="student-info">
            <div className="student-house">
              <img src={houseSelected[0].icon} alt="house" />
              {student.house}
            </div>
            <h2>{student.name}</h2>
            {!graded && (
              <>
                <input
                  type="number"
                  name="note"
                  onChange={(e) => handleInput(e.target.value)}
                />
                <div className="buttons">
                  <button className="gain" onClick={() => gradedAdd()}>
                    Gain
                  </button>
                  <button className="lose" onClick={() => gradedSub()}>
                    Lose
                  </button>
                </div>
              </>
            )}

            {graded && (
              <div className="graded">
                {add ? (
                  <>
                    <div className="note-add">+{inputValue}</div>
                    <button onClick={() => handleClose()}>Done</button>
                  </>
                ) : (
                  <>
                    <div className="note-sub">-{inputValue}</div>
                    <button onClick={() => handleClose()}>Done</button>
                  </>
                )}
              </div>
            )}
          </div>
        </div>
      </Modal>
    </div>
  );
};

export default ChangeNote;
