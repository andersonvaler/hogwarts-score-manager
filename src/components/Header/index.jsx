import hogwarts from "../../icons/hogwarts.svg";
const Header = () => {
  return (
    <header>
      <div className="logo">
        <figure>
          <img src={hogwarts} alt="logo" />
        </figure>
        <p>Hogwarts Score Manager</p>
      </div>
    </header>
  );
};
export default Header;
