const Cards = ({ house }) => {
  return (
    <div className="house">
      <h2>
        {house.index && "#" + house.index} {house.name}
      </h2>
      <img src={house.icon} alt={house.name} />
      <h4>{house.score}</h4>
    </div>
  );
};

export default Cards;
