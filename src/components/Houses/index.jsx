import { useSelector } from "react-redux";
import axios from "axios";
import { useEffect, useState } from "react";
import Cards from "../Cards";
import Students from "../Students";

const Houses = () => {
  const [students, setStudents] = useState();
  const houses = useSelector((state) => state.houses);

  useEffect(() => {
    axios
      .get("https://hp-api.herokuapp.com/api/characters/students")
      .then((response) => setStudents(response.data))
      .catch((e) => console.log(e));
  }, []);

  return (
    <>
      <div className="houses">
        {houses.map((house, index) => {
          return <Cards house={house} key={index} />;
        })}
      </div>
      <div className="students">
        <h2>Alunos</h2>
        <div className="title-column">
          <p>Nome</p>
          <p>Casa</p>
          <p></p>
        </div>
        {students &&
          students.map((student, index) => {
            return <Students student={student} key="index" />;
          })}
      </div>
    </>
  );
};

export default Houses;
