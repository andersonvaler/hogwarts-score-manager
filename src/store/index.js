import { createStore, combineReducers } from "redux";
import housesReducer from "./modules/houses/reducer";

const reducers = combineReducers({
  houses: housesReducer,
});

const store = createStore(reducers);

export default store;
