import gryffindor from "../../../icons/gryffindor.svg";
import slytherin from "../../../icons/slytherin.svg";
import ravenclow from "../../../icons/ravenclow.svg";
import hufflepuff from "../../../icons/hufflepuff.svg";

const defaulState = [
  { name: "Slytherin", score: 0, icon: slytherin },
  { name: "Gryffindor", score: 0, icon: gryffindor },
  { name: "Hufflepuff", score: 0, icon: hufflepuff },
  { name: "Ravenclaw", score: 0, icon: ravenclow },
];

const housesReducer = (state = defaulState, action) => {
  const add = () => {
    const editHouse = state.filter((house) => house.name === action.house);
    editHouse[0].score = editHouse[0].score + action.note;
    return sort(editHouse);
  };

  const sub = () => {
    const editHouse = state.filter((house) => house.name === action.house);
    editHouse[0].score = editHouse[0].score - action.note;
    return sort(editHouse);
  };

  const sort = (editHouse) => {
    const newList = state.filter((house) => house.name !== action.house);
    newList.push(editHouse[0]);
    newList.sort(function (a, b) {
      return a.score > b.score ? -1 : a.score < b.score ? 1 : 0;
    });
    for (let i = 0; i < newList.length; i++) {
      newList[i].index = i + 1;
    }
    return newList;
  };

  switch (action.type) {
    case "ADD_NOTE":
      return add();
    case "SUB_NOTE":
      return sub();
    default:
      return state;
  }
};

export default housesReducer;
