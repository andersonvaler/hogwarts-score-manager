export const addNote = (house, note) => ({ type: "ADD_NOTE", house, note });
export const subNote = (house, note) => ({ type: "SUB_NOTE", house, note });
