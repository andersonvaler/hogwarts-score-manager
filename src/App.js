import "./App.css";
import Header from "./components/Header";
import Houses from "./components/Houses";

function App() {
  return (
    <div className="App">
      <Header />
      <Houses />
    </div>
  );
}

export default App;
